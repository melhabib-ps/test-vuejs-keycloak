import Vue from 'vue';
import App from './App.vue';
import { createRouter } from './router';
import store from './store';
import { KeycloakService } from './keycloakservice';

Vue.config.productionTip = false;

KeycloakService.init().success((authenticated) => {
    // console.log(`%c I'm in initialization `, 'background: black; color: white');
    // console.log(`%c ${KeycloakService.keycloak.refreshToken}`, 'background: black; color: white');
    // console.log(`%c I'm in initialization - AccessToken `, 'background: black; color: white');
    // console.log(`%c ${KeycloakService.keycloak.token}`, 'background: black; color: white');
    // initialize vue app
    new Vue({
        router: createRouter(KeycloakService.keycloak),
        store,
        render: (h) => h(App),
    }).$mount('#app');
});
