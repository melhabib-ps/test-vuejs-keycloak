import Vue from 'vue';
import Keycloak, { KeycloakError, KeycloakInitOptions } from 'keycloak-js';
import VueResource from 'vue-resource';
import { config } from '@vue/test-utils';

const KEYCLOAK_COOKIE_NAME = 'KEYCLOAK_ADAPTER_STATE';
const KEYCLOAK_COOKIE_VALUE_SEPARATOR = '______';
Vue.use(VueResource);

export class KeycloakService {
    public static keycloak = Keycloak({
        'realm': 'keycloak-demo',
        'url': 'http://172.17.0.2:8080/auth',
        'ssl-required': 'none',
        'clientId': 'vue-test-app',
        'public-client': true,
        'confidential-port': 0,
    });
    public static init() {
        let keycloak = KeycloakService.keycloak
        // <-- keycloak events
        keycloak.onTokenExpired = () => {
            console.log('%c Token is expired', 'background: yellow; color: black');
            // your code
            KeycloakService.keycloak.updateToken(0).success((refreshed) => {
                if (refreshed) {
                    console.log('%c Token was successfully refreshed', 'background: green; color: white');
                    console.log('%c Token is refreshed - AccessToken', 'background: black; color: white');
                    console.log(`%c ${KeycloakService.keycloak.token}`, 'background: black; color: white');
                } else {
                    console.log('%c Token is still valid', 'background: green; color: white');
                }
            }).error(() => {
                console.log('%c Failed to refresh the token, or the session has expired', 'background: red; color: white');
            });
        };
        keycloak.onAuthRefreshSuccess = () => {
            console.log("AuthRefreshSuccess");
            KeycloakService.saveAuthCookie(KeycloakService.keycloak);
        };
        keycloak.onAuthSuccess = () => {
            console.log("AuthSuccess");
            KeycloakService.saveAuthCookie(KeycloakService.keycloak);
        }

        keycloak.onAuthLogout = () => {
            console.log("AuthLogout");
            KeycloakService.deleteAuthCookie(KeycloakService.keycloak);
        }
        Vue.http.interceptors.push((request: any, next: any) => {

            if (keycloak.authenticated) {
                request.headers.set('Authorization', 'Bearer '+ keycloak.token);
            }

            next((response: any) => {
                if (response.status == 401) {
                    keycloak.login({ redirectUri: window.location.href})
                }
            });
        });
        // -->
        let initOptions: KeycloakInitOptions = {
            checkLoginIframe: true
        }

        let tokenSet = KeycloakService.loadTokenSetFromAuthCookie();
        if (tokenSet) {
            // keycloak.authenticated = true
            // keycloak.token = tokenSet.access_token
            // keycloak.refreshToken = tokenSet.refresh_token
            // keycloak.tokenParsed = keycloak.decodeToken(token);
            // keycloak.sessionId = keycloak.tokenParsed.session_state;
            // keycloak.authenticated = true;
            // keycloak.subject = keycloak.tokenParsed.sub;
            // keycloak.realmAccess = keycloak.tokenParsed.realm_access;
            // keycloak.resourceAccess = keycloak.tokenParsed.resource_access;
            initOptions.token = tokenSet.access_token
            initOptions.refreshToken = tokenSet.refresh_token
        }
        let keycloakInstance = keycloak.init(initOptions);

        return keycloakInstance;
    }

    public static saveAuthCookie(kcs: Keycloak.KeycloakInstance) {
        (Vue as any).cookies.set(KEYCLOAK_COOKIE_NAME, `${kcs.token}${KEYCLOAK_COOKIE_VALUE_SEPARATOR}${kcs.refreshToken}`);
    }

    public static loadTokenSetFromAuthCookie() {
        return KeycloakService.toRawData((Vue as any).cookies.get(KEYCLOAK_COOKIE_NAME));
    }

    private static toRawData(keycloakAuth: string) {
        if (keycloakAuth && keycloakAuth.split(KEYCLOAK_COOKIE_VALUE_SEPARATOR).length === 2) {
            return {
                access_token: keycloakAuth.split(KEYCLOAK_COOKIE_VALUE_SEPARATOR)[0],
                refresh_token: keycloakAuth.split(KEYCLOAK_COOKIE_VALUE_SEPARATOR)[1],
            };
        } else {
            return null;
        }
    }

    public static deleteAuthCookie(kcs: Keycloak.KeycloakInstance) {
        (Vue as any).cookies.remove(KEYCLOAK_COOKIE_NAME);
    }

    public static login() {
        KeycloakService.keycloak.login();
    }

    public static logout() {
        KeycloakService.keycloak.logout();
    }

    public static isAuthenticated() {
        return KeycloakService.keycloak.authenticated;
    }

}
