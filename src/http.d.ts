// fix from : https://www.codewhoop.com/blog/vuejs/vue-resource-interceptors-with-typescript.html
import Vue from 'vue'

declare module 'vue/types/vue' {
  interface VueConstructor {
    http: any
  }
}