import Vue from 'vue';
// import Router from 'vue-router';
import VueRouter, { Location, Route, RouteConfig, RawLocation } from 'vue-router';
import Home from './views/Home.vue';
import MonCompte from './views/MonCompte.vue';
import Logout from './views/Logout.vue';
import VueCookies from 'vue-cookies';

Vue.use(VueCookies);
Vue.use(VueRouter);

export const createRoutes: (kcs: Keycloak.KeycloakInstance) => RouteConfig[] = (kcs: Keycloak.KeycloakInstance) => [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: { requiresAuth: false },
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
        meta: { requiresAuth: false },
        beforeEnter: (to, from, next) => {
            next();
        },
    },
    {
        path: '/moncompte',
        name: 'Mon Compte',
        component: MonCompte,
        meta: { requiresAuth: true },
        beforeEnter: (to, from, next) => { // TODO : make global and use meta.requiresAuth
            console.log('checkLogin');
            const minValidity = 2;
            // if (to.matched.some(record => record.meta.requiresAuth)) {
            if (!kcs.authenticated) {
                kcs.login({ redirectUri: window.location.origin + to.fullPath })
            } else if (kcs.isTokenExpired(minValidity)) {
                kcs.updateToken(minValidity);
            } else {
                console.log('checkLogin next');
                next();
            }
            // } else {
            //     next(); // make sure to always call next()!
            // }
        },
    },
    {
        path: '/logout',
        name: 'logout',
        component: Logout,
        meta: { requiresAuth: true },
        beforeEnter: (to, from, next) => { // TODO : make global and use meta.requiresAuth
            if (kcs.authenticated) {
                kcs.logout({ redirectUri: window.location.origin })
                    .error(function (errorData) {
                        console.log(JSON.stringify(errorData));
                    });
            } else {
                console.log('checkLogout next');
                next();
            }
            // } else {
            //     next(); // make sure to always call next()!
            // }
        },
    },
];

export const createRouter = (keycloakObj: Keycloak.KeycloakInstance) =>
    new VueRouter({
        mode: 'history',
        routes: createRoutes(keycloakObj),
        base: process.env.BASE_URL,
    });
